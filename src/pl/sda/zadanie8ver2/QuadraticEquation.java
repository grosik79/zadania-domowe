package pl.sda.zadanie8ver2;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;


    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        return (b * b) - (4 * a * c);
    }

    public double calculateX1() {
        double delta = calculateDelta();
        if (delta > 0) {
            return (-b - Math.sqrt(delta)) / (2 * a);
        } else if (delta == 0) {
            return -b / (2 * a);
        } else {
            System.out.println("Nie ma pierwiastków");
            return 0.0;
        }

    }

    public double calculateX2() {
        double delta = calculateDelta();
        if (delta > 0) {
            return (-b + Math.sqrt(delta)) / (2 * a);
        } else if (delta == 0) {
            System.out.println("Jest tylko x1");
            return calculateX1();
        } else {
            System.out.println("Nie ma pierwiastków");
            return 0.0;
        }
    }
}







