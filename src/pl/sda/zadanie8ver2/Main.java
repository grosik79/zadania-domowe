package pl.sda.zadanie8ver2;

public class Main {
    public static void main(String[] args) {
        QuadraticEquation equation = new QuadraticEquation(3,2,1);
        System.out.println(" Delta: " + equation.calculateDelta());
        System.out.println(" This is x1: " + equation.calculateX1());
        System.out.println(" This is x2: " + equation.calculateX2());
    }
}
