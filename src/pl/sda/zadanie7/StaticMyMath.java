package pl.sda.zadanie7;

public class StaticMyMath {
    public static int abs(int value){
        if (value < 0) return - value;
        return value;
    }
    public static double abs(double value){
        if(value < 0) return - value;
        return value;
    }
}

