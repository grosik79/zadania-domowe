package pl.sda.zadanie5;

public class Kwadrat {
private int a;

    public Kwadrat(int a) {
        this.a = a;
    }

    public double obliczPole() {
        return a*a;
    }

    public double obliczObwod() {
        return 4*a;
    }

}
