package pl.sda.zadanie5;

public class Main {
    public static void main(String[] args) {
        Kwadrat Ab = new Kwadrat(24);
        Prostokat Ap = new Prostokat(8,7);
        Kolo O = new Kolo(15);

        System.out.println("Obwód Kwadratu: " + Ab.obliczObwod());
        System.out.println("Pole Kwadratu: " + Ab.obliczPole());
        System.out.println("Obwód Prostokata: " + Ap.obliczObwod());
        System.out.println("Pole Prostokata: " + Ap.obliczPole());
        System.out.println("Obwód Koła: " + O.obliczObwod());
        System.out.println("Pole Koła: " + O.obliczPole());
    }
}
