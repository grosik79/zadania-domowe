package pl.sda.zadanie5;

public class Prostokat {
    private int a;
    private int b;

    public Prostokat(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public double obliczPole() {
        return a * b;
    }

    public double obliczObwod() {
        return (2 * a) + (2 * b);
    }
}
