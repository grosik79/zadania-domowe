package pl.sda.zadanie8;

import java.util.Scanner;

public class QuadraticEquation {
    public static void main(String[] args) {
        System.out.println("Podaj parametry równania kwadratowego ax2 + bx + c : ");
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        double delta = b * b - 4 * a * c;
        if(delta > 0){
            delta = Math.sqrt(delta);
            double x1 = (-b - delta)/(2*a);
            double x2 = (-b + delta)/(2*a);
            System.out.println("Pierwiastek równania: " + x1 + "oraz" + x2);
        }
        else if(delta == 0) {
            double x0 = -b/(2 * a);
            System.out.println("Pierwiastek podwójny równania to: " + x0);
        }
        else if (delta < 0) {
            System.out.println("Równianie nie posiada pierwiastków rzeczywistych ");
        }
     }

}
